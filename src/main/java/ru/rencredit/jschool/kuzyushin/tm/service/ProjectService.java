package ru.rencredit.jschool.kuzyushin.tm.service;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectIndexException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyNameException;
import ru.rencredit.jschool.kuzyushin.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findALl() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(name);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}
