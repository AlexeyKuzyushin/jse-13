package ru.rencredit.jschool.kuzyushin.tm.bootstrap;

import ru.rencredit.jschool.kuzyushin.tm.api.controller.ICommandController;
import ru.rencredit.jschool.kuzyushin.tm.api.controller.IProjectController;
import ru.rencredit.jschool.kuzyushin.tm.api.controller.ITaskController;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ICommandService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.constant.ArgumentConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.CommandConst;
import ru.rencredit.jschool.kuzyushin.tm.controller.CommandController;
import ru.rencredit.jschool.kuzyushin.tm.controller.ProjectController;
import ru.rencredit.jschool.kuzyushin.tm.controller.TaskController;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectArgException;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectCmdException;
import ru.rencredit.jschool.kuzyushin.tm.repository.CommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.ProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.TaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.service.CommandService;
import ru.rencredit.jschool.kuzyushin.tm.service.ProjectService;
import ru.rencredit.jschool.kuzyushin.tm.service.TaskService;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAILED]");
            }
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            parseArg(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAILED]");
        }
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                throw new IncorrectArgException(arg);
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.EXIT:
                commandController.exit();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            default:
               throw new IncorrectCmdException(command);
        }
    }
}
